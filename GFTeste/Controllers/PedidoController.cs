﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GFTeste.Models;
using GFTeste.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace GFTeste.Controllers
{
    [Route("api/[controller]")]
    public class PedidoController : Controller
    {
        readonly IPedidoRepository repository;

        PedidoController(IPedidoRepository repository) {
            this.repository = repository;
        }

        [HttpGet]
        public async Task<IEnumerable<Pedido>> Get()
        {
            return await repository.PegarTodos();
        }

        [HttpGet("{id}/pedidos")]
        public Pedido GetPedidos(int id)
        {
            return repository.PegarPorId(id);
        }

        [HttpGet("{id}")]
        public Pedido Get(int id)
        {
            return repository.PegarPorId(id);
        }

        [HttpPost]
        public void Post([FromBody]Pedido fornecedor)
        {
            repository.Inserir(fornecedor);
            repository.Salvar();
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Pedido fornecedor)
        {
            fornecedor.Id = id;
            repository.Editar(fornecedor);
            repository.Salvar();
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            repository.Excluir(id);
        }
    }
}
