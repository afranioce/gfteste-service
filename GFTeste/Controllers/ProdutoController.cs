﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GFTeste.Models;
using GFTeste.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace GFTeste.Controllers
{
    [Route("api/[controller]")]
    public class ProdutoController : Controller
    {
        readonly IProdutoRepository repository;

        ProdutoController(IProdutoRepository repository) {
            this.repository = repository;
        }

        [HttpGet]
        public async Task<IEnumerable<Produto>> Get()
        {
            return await repository.PegarTodos();
        }

        [HttpGet("{id}")]
        public Produto Get(int id)
        {
            return repository.PegarPorId(id);
        }

        [HttpPost]
        public void Post([FromBody]Produto fornecedor)
        {
            repository.Inserir(fornecedor);
            repository.Salvar();
        }

        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Produto fornecedor)
        {
            fornecedor.Id = id;
            repository.Editar(fornecedor);
            repository.Salvar();
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            repository.Excluir(id);
        }
    }
}
