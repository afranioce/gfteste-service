﻿using System;
using System.Threading.Tasks;
using GFTeste.Models;
using GFTeste.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace GFTeste.Controllers
{
    [Route("api/[controller]")]
    public class FornecedorController : Controller
    {
        readonly IFornecedorRepository repository;

        FornecedorController(IFornecedorRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Get()
        {
            var forenecedores = await repository.PegarTodos();
            return Ok(forenecedores);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        public IActionResult Get(int id)
        {
            var fornecedor = repository.PegarPorId(id);
            if (fornecedor == null)
            {
                NotFound();
            }

            return Ok(fornecedor);
        }

        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult Post([FromBody]Fornecedor fornecedor)
        {
            repository.Inserir(fornecedor);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                repository.Salvar();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }

            return Ok(fornecedor);
        }

        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public IActionResult Put(int id, [FromBody]Fornecedor fornecedor)
        {
            fornecedor.Id = id;
            repository.Editar(fornecedor);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                repository.Salvar();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }

            return Ok(fornecedor);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        public IActionResult Delete(int id)
        {
            repository.Excluir(id);
            return NoContent();
        }
    }
}
