﻿using GFTeste.Models;
using GFTeste.Repositories;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using SimpleInjector;

namespace GFTeste
{
    public class Program
    {
        static readonly Container container;

        static Program()
        {
            container = new Container();

            container.Register<DbContext, GFTesteDbContext>();
            container.Register<IFornecedorRepository, FornecedorRepository>();
            container.Register<IPedidoRepository, PedidoRepository>();
            container.Register<IProdutoRepository, ProdutoRepository>();

            container.Verify();
        }

        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
