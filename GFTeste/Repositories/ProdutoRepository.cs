﻿using GFTeste.Models;

namespace GFTeste.Repositories
{
    public class ProdutoRepository : RepositoryBase<Produto>, IProdutoRepository
    {
        ProdutoRepository(GFTesteDbContext context) : base(context)
        {

        }
    }
}
