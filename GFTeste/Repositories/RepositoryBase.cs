﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using GFTeste.Models;
using Microsoft.EntityFrameworkCore;

namespace GFTeste.Repositories
{
    public abstract class RepositoryBase<TModel> : IRepositoryBase<TModel>, IDisposable where TModel : class
    {
        protected readonly GFTesteDbContext context;
        DbSet<TModel> dbSet;

        public RepositoryBase(GFTesteDbContext context)
        {
            this.context = context;
            dbSet = context.Set<TModel>();
        }

        public virtual TModel PegarPorId(int id)
        {
            return dbSet.Find(id);
        }

        public virtual void Editar(TModel model)
        {
            dbSet.Attach(model);
            context.Entry(model).State = EntityState.Modified;
        }

        public virtual void Inserir(TModel model)
        {
            dbSet.Add(model);
        }

        public virtual void Excluir(int id)
        {
            var model = dbSet.Find(id);
            Excluir(model);
        }

        public virtual void Excluir(TModel model)
        {
            if (context.Entry(model).State == EntityState.Detached)
            {
                dbSet.Attach(model);
            }
            dbSet.Remove(model);
        }

        public virtual async Task<IEnumerable<TModel>> PegarTodos()
        {
            return await dbSet.ToListAsync();
        }

        public void Salvar()
        {
            context.SaveChanges();
        }

        bool disposed;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
