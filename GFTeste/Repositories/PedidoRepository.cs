﻿using GFTeste.Models;

namespace GFTeste.Repositories
{
    public class PedidoRepository : RepositoryBase<Pedido>, IPedidoRepository
    {
        PedidoRepository(GFTesteDbContext context) : base(context)
        {
            
        }
    }
}
