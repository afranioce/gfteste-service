﻿using GFTeste.Models;

namespace GFTeste.Repositories
{
    public interface IProdutoRepository : IRepositoryBase<Produto>
    {
    }
}
