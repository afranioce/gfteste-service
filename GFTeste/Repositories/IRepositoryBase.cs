﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GFTeste.Repositories
{
    public interface IRepositoryBase<TModel>
    {
        void Inserir(TModel model);
        void Editar(TModel model);
        void Excluir(int id);
        void Excluir(TModel model); 
        Task<IEnumerable<TModel>> PegarTodos();
        TModel PegarPorId(int id);
        void Salvar();

    }
}
