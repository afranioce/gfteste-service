﻿using GFTeste.Models;

namespace GFTeste.Repositories
{
    public interface IPedidoRepository : IRepositoryBase<Pedido>
    {
    }
}
