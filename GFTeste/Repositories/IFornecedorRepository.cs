﻿using GFTeste.Models;

namespace GFTeste.Repositories
{
    public interface IFornecedorRepository : IRepositoryBase<Fornecedor>
    {
    }
}
