﻿using GFTeste.Models;

namespace GFTeste.Repositories
{
    public class FornecedorRepository : RepositoryBase<Fornecedor>, IFornecedorRepository
    {
        FornecedorRepository(GFTesteDbContext context) : base(context)
        {
            
        }
    }
}
