﻿using System;
namespace GFTeste.Models
{
    public class Produto
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
        public DateTime CadastradoEm { get; set; }
        public float Valor { get; set; }
    }
}
