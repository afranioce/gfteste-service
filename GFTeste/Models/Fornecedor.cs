﻿using System;
using System.Collections.Generic;

namespace GFTeste.Models
{
    public class Fornecedor
    {
        public int Id { get; set; }
        public string RazaoSocial { get; set; }
        public string Cnpj { get; set; }
        public string EmailContato { get; set; }
        public string NomeContato { get; set; }
        public IList<Pedido> Pedidos { get; set; }
    }
}
