﻿using System;
using System.Collections.Generic;

namespace GFTeste.Models
{
    public class Pedido
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public int FornecedorId { get; set; }
        public float Valor { get; set; }
        public List<PedidoProduto> PedidoProdutos { get; set; }
        public Fornecedor Fornecedor { get; set; }
    }
}
