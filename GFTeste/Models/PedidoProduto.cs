﻿using System;
namespace GFTeste.Models
{
    public class PedidoProduto
    {
        public int PedidoId { get; set; }
        public int ProdutoId { get; set; }
        public float Valor { get; set; }
        public int Quantidade { get; set; }
        public Pedido Pedido { get; set; }
        public Produto Produto { get; set; }
    }
}
